###### O cliente pode fazer
+ Cadastrar, Editar e Encerrar sua Conta OK
+ Realizar depósitos OK
+ Realizar saques OK
+ Realizar transferências entre contas PENDENTE
+ Solicitar saldo OK
+ Solicitar extrato filtrando por data início e final OK

###### Regras:
+ Para realizar saques e transferências é necessário autenticar o usuário
+ O saldo de uma conta nunca pode ficar negativo

###### Taxa de transferência:
+ De segunda a sexta das 9 às 18 horas a taxa é 5 reais por transferência
+ Fora desse horário a taxa é 7 reais
+ Acima de 1000 reais há um adicional de 10 reais na taxa

###### Exigências:
+ Fazer testes unitários e de aceitação. Rspec, TestUnit, ou o que preferir.
+ Versionar o código com git
+ Hospedar a aplicação no heroku
+ Enviar o código por email em um arquivo zip (contendo os arquivos de versionamento do git)

> FEITO

